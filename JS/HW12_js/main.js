let imgs = document.getElementById('imgs');
let stop = document.getElementById('stop');
let proceed = document.getElementById('proceed');
stop.classList.remove('vis');
proceed.classList.add('vis');
let imgsVis = function(imgs){
    for (let i = 0; i < imgs.children.length; i++) {
        imgs.children[i].classList.add('vis');
    }};
imgsVis(imgs);
let currentImg = 0;
imgInterval = setInterval(nextImg,500);
imgs.children[0].classList.remove('vis');
function nextImg() {
    imgs.children[currentImg].classList.add('vis');
    currentImg = (currentImg+1)%imgs.children.length;
    imgs.children[currentImg].classList.remove('vis');
}
let stopShow = function() {
    clearInterval(imgInterval);
    proceed.classList.remove('vis');
};
let proceedShow = function() {
    imgInterval = setInterval(nextImg,500);
    stop.classList.remove('vis');
    proceed.classList.add('vis');
};
stop.addEventListener("click", stopShow);
proceed.addEventListener("click", proceedShow);
