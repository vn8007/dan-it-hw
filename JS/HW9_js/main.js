let navi = document.getElementById('tabs');
let text = document.getElementById('text');

let displayNoneText = function () {
    for (let elem of text.children)
    {elem.classList.add('active-li')}
};

let idForNaviAndText = function () {
    for (let i = 0; i < text.children.length; i++)
    {text.children[i].dataset.id = i;}
    for (let i = 0; i < navi.children.length; i++)
    {navi.children[i].dataset.id = i;}
};
navi.onclick = function(event) {
    const li = event.target;
    for (const elem of this.children) {
        elem.classList.remove('active');
    }
    li.classList.add('active');
};

let onClickEvent = function(event) {
    setActiveLiText (event.target);
};

let setActiveLiText = function (event) {
    for(let i = 0; i < text.children.length; i++) {
        if(text.children[i].dataset.id === event.dataset.id){
            text.children[i].classList.remove('active-li');
            continue;
        }
        text.children[i].classList.add('active-li');
    }
};
navi.addEventListener("click", onClickEvent);
displayNoneText();
idForNaviAndText();