let input1 = document.querySelector('#one');
let input2 = document.querySelector('#two');
let eye1 = document.getElementById('i1');
let eye2 = document.getElementById('i2');
let but = document.getElementById('but');


eye1.classList.remove('fa-eye-slash');
eye1.classList.add('fa-eye');
eye2.classList.remove('fa-eye-slash');
eye2.classList.add('fa-eye');

let showPass1 = function (target) {
    let input1 = document.getElementById('one');
    if (input1.getAttribute('type') === 'password') {
        eye1.classList.add('fa-eye-slash');
        input1.setAttribute('type', 'text');
    } else {
        eye1.classList.remove('fa-eye-slash');
        input1.setAttribute('type', 'password');
    }
    return false;
};

let showPass2 = function (target) {
    let input2 = document.getElementById('two');
    if (input2.getAttribute('type') === 'password') {
        eye2.classList.add('fa-eye-slash');
        input2.setAttribute('type', 'text');
    } else {
        eye2.classList.remove('fa-eye-slash');
        input2.setAttribute('type', 'password');
    }
    return false;
};

let button = function(){
    if (input1.value === input2.value)
    {alert('You are welcome')}
    else {
        let place = document.querySelector('body');
        let par = document.createElement('p');
        par.innerText = 'Нужно ввести одинаковые значения';
        place.append(par);
    }
};

eye1.onclick = showPass1;
eye2.onclick = showPass2;
but.onclick = button;