let input = document.querySelector('input'),
    span = document.createElement('span'),
    forSpan = document.getElementById('inner'),
    div = document.getElementById('button'),
    imgClose = document.querySelector('img');

div.classList.add('for-span');
span.classList.add('for-span');
document.body.append(span);

input.onfocus = function() {
    span.classList.add('for-span');
    span.classList.remove('color-text-error');
    input.classList.remove('input-error', 'color-text');
    input.classList.add('input')};
input.onblur = function() {
    let data = +document.getElementsByTagName("input")[0].value;
    input.classList.remove('input');
    if (data > 0) {
        forSpan.innerText = `Current price: ${data}`;
        div.classList.remove('for-span');
        input.classList.add('color-text');
    }
    else if (data === 0){
        span.innerText = `Please enter price!!!`;
        div.classList.add('for-span');
        span.classList.remove('for-span');
        span.classList.add('color-text-error');
        input.classList.add('input-error');
    }
    else {
        span.innerText = `Please enter correct price!!!`;
        span.classList.remove('for-span');
        div.classList.add('for-span');
        span.classList.add('color-text-error');
        input.classList.add('input-error');
    }};


let bot = function(){
    let inputs = document.querySelectorAll('input[type=text]');
    for (let i = 0;  i < inputs.length; i++) {inputs[i].value = '';}
    div.classList.add('for-span');
    input.classList.remove('input-error', 'color-text');
    input.classList.add('input')};
imgClose.onclick = bot;
