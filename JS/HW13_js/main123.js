let dark = document.getElementById('dark');
let light = document.getElementById('light');
let body = document.querySelector('body');
let form = document.getElementById('form');
let input = document.querySelectorAll('.input-form');
let btn = document.getElementById('btn');
let facebook = document.getElementById('facebook');
let google = document.getElementById('google');
let svg = document.getElementsByClassName('svg-link-color');
let jorney = document.getElementById('jorney');
let welcome = document.getElementById('welcome');
let log = document.getElementById('log');
let sign = document.getElementById('sign');

let darkTheme = function () {
    body.classList.add('background-style-dark');
    form.style.background = 'gray';
    btn.style.color = '#3f3f3f';
    facebook.style.color = '#3f3f3f';
    google.style.color = '#3f3f3f';
    jorney.style.color = '#3f3f3f';
    welcome.style.color = '#3f3f3f';
    log.style.color = '#3f3f3f';
    sign.style.color = '#3f3f3f';
    dark.classList.add('display-btn');
    light.classList.remove('display-btn');

    input = Array.prototype.slice.call(input);
    input.forEach(function(input) {
    input.tagName;});
    for(const i in input){
    input[i].style.background = '#8A8A8A';
    input[i].style.border = '1px solid #3f3f3f';}

    svg = Array.prototype.slice.call(svg);
    svg.forEach(function(svg) {
    svg.tagName;});
    for(const i in svg){
     svg[i].style.fill = '#3f3f3f';}

    jorney.onmouseenter = function () {
    jorney.style.color = '#3CB878'};
    jorney.onmouseout= function () {
    jorney.style.color = '#3f3f3f'};

    log.onmouseenter = function (){
    log.style.color = '#3CB878'};
    log.onmouseout= function () {
    log.style.color = '#3f3f3f'};

    sign.onmouseenter = function () {
    sign.style.color = '#3CB878'};
    sign.onmouseout= function () {
    sign.style.color = '#3f3f3f'};

    light.onmouseenter = function () {
    light.style.color = '#3CB878'};
    light.onmouseout= function () {
    light.style.color = '#3f3f3f'};
};
let func = localStorage.getItem('func');
let yourFunc = eval('(' + func + ')');

let theme = function(){
    if(localStorage.getItem('func') !== null){
        yourFunc();
  }};
theme();
let local = function(){ localStorage.setItem('func', darkTheme.toString())};
dark.addEventListener("click", local);
dark.addEventListener("click", darkTheme);
let reload = function() {window.location.reload()};
let delLocal = function (){localStorage.clear()};
light.addEventListener("click", delLocal);
light.addEventListener("click", reload);