const express = require('express');
const ConfigService = require('./helpers/config.service');
const { connectToDatabase } = require('./helpers/db.helper');
const routersRegister = require('./api/index');

// ConfigService.init();
const app = express();

async function main() {
  routersRegister(app);

  await connectToDatabase();

  const port = ConfigService.get('APP_PORT');
  app.listen(port, () => {
    console.log(`App start on port ${port}`);
  });
}

main();
