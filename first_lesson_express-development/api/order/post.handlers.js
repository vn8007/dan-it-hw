const Order = require('../../models/order.model');
const Product = require('../../models/product.model');
const mongoose = require('mongoose');
exports.createOrder = async (req, res) => {
  const products = req.body.products;

  const user = req.user;

  let totalPrice = 0;
  const mappedProducts = await Promise.all(
    products.map(async (product) => {
      const productFromDB = await Product.findById(product.id).exec();
      totalPrice += productFromDB.price * product.quantity;
      return { ...product, id: mongoose.Types.ObjectId(product.id) };
    })
  );

  const order = new Order({
    user: user._id,
    totalPrice,
    createdAt: new Date(),
    products: mappedProducts,
    isPayed: false,
  });

  await order.save();

  user.orders.push(order);
  await user.save();

  res.send(order).end();
};
