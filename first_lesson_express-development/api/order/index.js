const express = require('express');
const router = express.Router();

const { createOrder } = require('./post.handlers');
//
router.post('/', createOrder);

module.exports = router;
