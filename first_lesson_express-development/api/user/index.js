const express = require('express');
const router = express.Router();

const { getUserById, whoami, getOwnOrders } = require('./get.handlers');

const {paymentComfirmation} = require('./patch.hendlers')

router.get('/whoami', whoami);
router.get('/orders', getOwnOrders);
router.get('/:id', getUserById);


module.exports = router;
