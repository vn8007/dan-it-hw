const User = require('../../models/user.model')

exports.paymentComfirmation = async (req, res) =>{
    const {orderId} = req.params;
    const user = req.user;
    const order = await Order.findById(orderId).populate('user').exec();


    if(!order){
        res.status(400).send(`Not found ${orderId}!`).end()
    }

    if(order.isPayed){
        res.status(400).send(`Order is payed already!`).end()
    }

    if(user.balance < order.totalPrice){
        res.status(400).send(`Not found!`).end()
    }

    if(String(order.user) !== String(user.id)){
        res.status(400).send(`Error`).end()
    }

    user.balance -= order.totalPrice;

    await user.save();

    order.isPayed = true;
    res.status(200).send(`Order with id ${orderId} was payed successfully!`)

}