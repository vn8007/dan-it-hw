const User = require('../../models/user.model');

exports.getUserById = async (req, res) => {
  const user = await User.findById(req.params.id).exec();

  if (!user) {
    res.status(404).send('User not found').end();
  }

  res.send(user).end();
};

exports.whoami = async (req, res) => {
  res.send(req.user);
};

exports.getOwnOrders = async (req, res) => {
  const { orders } = await User.findById(req.user._id)
    .populate('orders')
    .exec();
  res.status(200).send(orders);
};
