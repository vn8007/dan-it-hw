const express = require('express');
const fileUpload = require('express-fileupload');

const authRouter = require('./auth/index');
const orderRouter = require('./order/index');
const productRouter = require('./product/index');
const userRouter = require('./user/index');
const { authMiddleware } = require('./auth/auth.handlers');

module.exports = (app) => {
  app.use(express.json());
  app.use(fileUpload());
  app.use(express.static('public'));

  app.use('/auth', authRouter);
  app.use('/', authMiddleware);

  app.use('/products', productRouter);
  app.use('/orders', orderRouter);
  app.use('/users', userRouter);
};
