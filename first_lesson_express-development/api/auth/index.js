const express = require('express');
const router = express.Router();
const { validationMiddleware } = require('../../helpers/validation.middleware');
const { registrationValidationSchema } = require('./validation.schema');

const { login, registration } = require('./auth.handlers');

router.post('/login', login);
router.post(
  '/registration',
  validationMiddleware(registrationValidationSchema),
  registration
);

module.exports = router;
