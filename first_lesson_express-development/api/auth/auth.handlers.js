const jwt = require('jsonwebtoken');
const User = require('../../models/user.model');
const { pick } = require('../../helpers/common.tools');
const ConfigService = require('../../helpers/config.service');
const secretKey = ConfigService.get('SECRET_KEY') || 'Secret  ';

exports.login = async (req, res) => {
  const { email, password } = req.body;

  const user = await User.findOne({ email }).exec();

  const userNotFound = !user;

  const passwordsNotMatch = user && user.password !== password;

  if (userNotFound || passwordsNotMatch) {
    res.status(401).send({
      message: userNotFound ? 'User not found!' : 'Passwords not match!',
    });
    //
    return;
  }
  //
  const token = jwt.sign(pick(user, ['email', 'profile']), secretKey, {
    expiresIn: '3h',
  });

  res.send({ token }).end();
};

exports.registration = async (req, res) => {
  const userData = pick(req.body, [
    'age',
    'password',
    'email',
    'profile',
    'sex',
    'isMarried',
  ]);
  userData.profile = pick(req.body.profile, [
    'username',
    'firstName',
    'lastName',
  ]);

  const usernameInUsed = !!(await User.findOne({
    profile: { username: userData.username },
  }).exec());
  const emailInUsed = !!(await User.findOne({ email: userData.email }).exec());

  if (usernameInUsed || emailInUsed) {
    res
      .status(400)
      .send(
        `${
          usernameInUsed
            ? 'That username already in use.Choose other,please.'
            : ''
        }.${
          emailInUsed ? 'That username already in use.Choose other,please.' : ''
        }`
      );
  }

  const user = new User(userData);

  await user.save();

  res.status(201).send('User was successfully registered!');
};

exports.authMiddleware = async (req, res, next) => {
  const token = (req.headers.authorization || '').split(' ')[1];

  if (!token) {
    return res.status(403).send('A token is required for authentication');
  }
  try {
    const decoded = jwt.verify(token, secretKey);
    const user = await User.findOne({ email: decoded.email }).exec();
    req.user = user;
  } catch (err) {
    return res.status(401).send('Invalid Token');
  }
  return next();
};
