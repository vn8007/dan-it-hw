exports.registrationValidationSchema = {
  age: { type: 'number', min: 5, max: 99 },
  password: { type: 'string', min: 3 },
  email: { type: 'string', min: 10 },
  profile: {
    type: 'object',
    schema: {
      username: { type: 'string', min: 5 },
      firstName: { type: 'string', min: 3 },
    },
  },
  sex: { type: 'string', isEnum: true, allowedValues: ['MALE', 'FEMALE'] },
};
