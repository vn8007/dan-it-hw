const Product = require('../../models/product.model');

exports.deleteProductById = async (req, res) => {
  const product = await Product.findById(req.params.id).exec();

  if (!product) {
    res.status(404).send('User not found').end();
  }

  await Product.findByIdAndDelete(req.params.id).exec();

  res.send('Product was delete.').end();
};
