const Product = require('../../models/product.model');

exports.updateProductById = async (req, res) => {
  const product = await Product.findById(req.params.id).exec();

  if (!product) {
    res.status(404).send('User not found').end();
  }

  await Product.findByIdAndUpdate(req.params.id, req.body).exec();

  res.send('Product was update').end();
};
