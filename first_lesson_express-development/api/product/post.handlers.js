const path = require('path');
const { pick } = require('../../helpers/common.tools');
const Product = require('../../models/product.model');
exports.uploadProductImage = async (req, res) => {
  const file = req.files.sample;

  file.name = `${req.params.id}.${file.name.split('.')[1]}`;
  const uploadPath = path.join(__dirname, '..', '..', 'public', file.name);
  await file.mv(uploadPath);

  res.send('File was upload!');
};

exports.createProduct = async (req, res) => {
  const productData = pick(req.body, ['price', 'description', 'name']);

  const product = new Product(productData);
  await product.save();
  res.status(200).send(product);
};
