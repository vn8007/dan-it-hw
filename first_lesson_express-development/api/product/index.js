const express = require('express');
const router = express.Router();

const { getProductById, getProducts } = require('./get.handlers');
const { deleteProductById } = require('./delete.handlers');
const { updateProductById } = require('./patch.handlers');
const { uploadProductImage, createProduct } = require('./post.handlers');

router.get('/:id', getProductById);
router.get('/', getProducts);
router.delete('/:id', deleteProductById);
router.patch('/:id', updateProductById);
router.post('/:id', uploadProductImage);
router.post('/', createProduct);

module.exports = router;
