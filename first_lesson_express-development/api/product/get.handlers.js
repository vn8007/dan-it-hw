const Product = require('../../models/product.model');

exports.getProductById = async (req, res) => {
  const product = await Product.findById(req.params.id).exec();

  if (!product) {
    res.status(404).send('User not found').end();
  }

  res.send(product).end();
};

exports.getProducts = async (req, res) => {
  const products = await Product.find({
    name: !req.query.name ? /.*/i : new RegExp(req.query.name, 'i'),
  }).exec();

  res.send(products).end();
};
