const validateProperties = (requestData, objectSchema) => {
  Object.keys(objectSchema).forEach((key) => {
    const {
      type,
      min = Number.NEGATIVE_INFINITY,
      max = Number.POSITIVE_INFINITY,
      schema,
      allowedValues = [],
    } = objectSchema[key];

    const validatedProperty = requestData[key];

    if (!validatedProperty || typeof validatedProperty !== type) {
      throw new Error(`Property ${key} is required!`);
    }

    if (allowedValues.length) {
      if (!allowedValues.includes(validatedProperty)) {
        throw new Error(`Allowed values - ${allowedValues.join(',')}`);
      }
    }

    const isValueInInterval = (value) => min <= value && value <= max;

    if (['string', 'number'].includes(type)) {
      const isValidated = isValueInInterval(
        type === 'string' ? validatedProperty.length : validatedProperty
      );

      if (!isValidated) {
        throw new Error(
          `Value size for property ${key} must be in interval {min:${min},Max:${max}}`
        );
      }
    }

    if (type === 'object') {
      validateProperties(validatedProperty, schema);
    }

    return true;
  });
};

exports.validationMiddleware = (objectSchema) => {
  return async (req, res, next) => {
    try {
      validateProperties(req.body, objectSchema);
      next();
    } catch (err) {
      res.status(404).send(err.message);
    }
  };
};
