const mongoose = require('mongoose');

const ProductSchema = mongoose.Schema({
  price: Number,
  description: String,
  name: String,
});

module.exports = mongoose.model('Product', ProductSchema);
