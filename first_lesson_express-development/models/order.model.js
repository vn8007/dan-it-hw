const mongoose = require('mongoose');
const OrderSchema = mongoose.Schema({
  totalPrice: Number,
  createdAt: Date,
  user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  products: [
    {
      product: { type: mongoose.Schema.Types.ObjectId, ref: 'Product' },
      quantity: Number,
    },
  ],
  isPayed: Boolean,
});
module.exports = mongoose.model('Order', OrderSchema);
