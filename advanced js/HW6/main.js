const url = 'https://api.ipify.org/?format=json';
const urlIp = `http://ip-api.com/json`;
const btn = document.getElementById('btn');
let creatRes = (obj, ipAd) =>
{
    let p = document.createElement('p');
    p.innerHTML = `Континент - ${obj.timezone},<br/>
                          страна - ${obj.country},<br/>
                          регион - ${obj.regionName},<br/>
                           город - ${obj.city},<br/>
                    район города - ${obj.region},<br/>
                               IP: ${ipAd}`;
    document.body.append(p);
};
let ipFunc = async ()=>{
    let ipPromise = await fetch(url);
    let ip = await ipPromise.json();
    console.log(ip);
    const ipAd = ip.ip ;
    console.log(ipAd);
    let url2 = [urlIp,ipAd].join('/');
    let ipAdd = await fetch(url2);
    let ipLoc = await ipAdd.json();
    console.log(ipLoc);
    creatRes(ipLoc, ipAd);
};
btn.addEventListener('click', ipFunc);