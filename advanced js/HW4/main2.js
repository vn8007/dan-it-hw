const url = 'https://ajax.test-danit.com/api/swapi/films';
const ul = document.createElement('ul');


const getData = async function (string) {
    const response = await fetch(string);
    const films = await response.json();
    console.log(films);
    getFilms(films);

};

const getFilms = (array) => array.map(item =>{
    ul.append(...array.map(item => {

        const li = document.createElement('li');
        li.innerHTML = `<b>Title</b> ${item.name} </br>
                        <b>Episode</b> ${item.episodeId} </br>
                        <b>Crawl</b> ${item.openingCrawl}</br>
                        <b>Characters</b> ${getActors(item.characters)}`;
        console.log(item.characters);
        return li
    }))});

const getActors = async function(array){
    const res = await array.map(item =>  fetch(item));
    console.log(res);
    const act = res.map(item => item.json());
    console.log(act);

    ul.append(...act.map(item => {
        const li = document.createElement('li');
        li.innerHTML = `${item.name}`
    }));
    return li
};

document.body.append(ul);
const rest = getData(url);
console.log(rest);
//
// (async() => {
//     const response = await fetch('https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json');
//     const films = await response.json();
//     console.log(films);
//
//     fetch('https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json')
//         .then(data => data.json())
//         .then(films => {
//             return [{ id, name, heroes: [1,2,3]}]
//         })
//         .then(ids => {
//             return fetch(ids)
//         })
//         .then(data => data.json())
//         .then(heroes => {
//             document.write();
//         })
//     ;
// })();
