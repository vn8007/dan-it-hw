const films = 'https://ajax.test-danit.com/api/swapi/films';
const getData = (string) => fetch(string).then(response => response.json());
const ul = document.createElement('ul');
const  creatFilms = (collection) => {
    ul.append(...collection.map(item => {
        const li = document.createElement('li');
        li.innerHTML = `<b>Title</b> ${item.name} </br>
                        <b>Episode</b> ${item.episodeId} </br>
                        <b>Crawl</b> ${item.openingCrawl}</br>
                        <b>Characters</b> `;
        characters(item.characters).then(d => creatActors(d)).then(d => li.append(d.join(', ')));
        return li;}))};
const creatActors = (collection) => {
    return collection.map(item=>
    item.name);
};
const characters = (collection) => {
    return Promise.all(collection.map(item => getData(item)));
};
document.body.append(ul);
getData(films).then(res => creatFilms(res));

