const bookss = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
}, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
}, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
}];
const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
};
let allBooks = [...bookss, bookToAdd];
console.log(allBooks);
