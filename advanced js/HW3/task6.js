const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
};

const newEmployee = {
    ...employee,
    age: 45,
    salary: 100500,
};
console.log(newEmployee);
