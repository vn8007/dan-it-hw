const array = ['value', () => 'showValue'];
const [value, showValue] = ['value', () => 'showValue']

alert(value); // должно быть выведено 'value'
alert(showValue());  // должно быть выведено 'showValue'
