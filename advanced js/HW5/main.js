const urlUser = 'https://ajax.test-danit.com/api/json/users';
const urlPosts = 'https://ajax.test-danit.com/api/json/posts';

class Card {
    constructor(
        name, email, title, body, id, userId){
        this.name = name;
        this.email = email;
        this.id = id;
        this.title = title;
        this.body = body;
        this.userId = userId;
    }
}

let creatPosts = (posts) =>{posts.map(item =>{
    document.body.append(...posts.map(item =>{
        const div = document.createElement('div');
        div.classList.add('twit-item');
        div.innerHTML = `<p>${item.body}</p>
                         <p>${item.title}</p>`;
        return div}))})};

(async ()=>{
    let getUser = await fetch(urlUser);
    let users = await getUser.json();
    console.log(users);

    let getPosts = await fetch(urlPosts);
    let posts = await getPosts.json();
    console.log(posts);

    creatPosts(posts);
})();
