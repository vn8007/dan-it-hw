const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",

    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];
const ul = document.createElement('ul');
const place = document.getElementById('root');
const newBook = function(array) {
    array.map(function(item) {
        try{
            if(item.name && item.author && item.price) {
                const li = document.createElement('li');
                li.innerHTML = `Название книги: ${item.name} <br> Автор: ${item.author} <br> Цена: ${item.price}$`;
                ul.append(li);
            }
            else {
                if (item.name === undefined)
                    throw `no name in ${item.author} ${item.price}`;
                if (item.author === undefined)
                    throw `no author in ${item.name} ${item.price}`;
                if (item.price === undefined)
                    throw `no price in ${item.author} ${item.name}`;
            }
        }
        catch(error) {
            console.error(error);
        }
    });
    place.append(ul);
};
newBook(books);