import actions from "./actions";
import reducer from "../favorite/reducers";

const initialState = {
    data: []
};

describe('Test favorite reducer',() =>{

    const favorite = [
        123, 12134]
    test ('SET_FAVORITE to data',()=>{
        const action = actions.setFavorite(favorite);
        const newState = reducer(initialState, action)
        expect(newState.data).toBe(favorite)
    })
})