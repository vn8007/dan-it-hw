import types from "./types";



const initialState = {
        data: []
};




const reducer = (state = initialState, action) => {
    switch (action.type) {
        case types.SET_CART: {
            return {...state, data: action.data}
        }

    default: return state
    }
}
export default reducer;