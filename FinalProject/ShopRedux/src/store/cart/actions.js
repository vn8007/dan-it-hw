import types from "./types";

const setCart = (cartData) => ({type: types.SET_CART, data: cartData})

export default {
    setCart,
}