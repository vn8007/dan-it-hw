const SET_CART = 'shop/cart/SET_CART';

export default {
    SET_CART,
}