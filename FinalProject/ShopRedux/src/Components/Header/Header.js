import React from 'react';
import './Header.css'
import {connect} from "react-redux";


function Header({cart, favorite}) {
return (
        <div>
            <header className='background'>
                <div className='top-nav'>
                    <div>
                        <img src='logo192.png' width='50px'/>
                    </div>
                    <div>
                        <ul className='main-menu'>
                            <li className='main-menu__item'>Главная</li>
                            <li className='main-menu__item'>Галерея</li>
                            <li className='main-menu__item'><a href='/shop' className='link-style'>Магазин</a></li>
                            <li className='main-menu__item'>Контакты</li>
                        </ul>
                    </div>
                    <div className='main-menu-bay'>
                        <p className='main-menu-bay__item'>
                            <a href='/cart' className='link-style'>корзина
                                <span className='counter'>{!cart? '0' : cart.length}
                                </span>
                            </a>
                        </p>
                        <p className='main-menu-bay__item'>
                            <a href='/favorite' className='link-style'>избранное
                                <span className='counter'>{!favorite? '0' : favorite.length}
                                </span>
                            </a>
                        </p>
                    </div>

                </div>
                {/*<h2 className='name'>Магазин изделий из кожи ручной работы</h2>*/}
            </header>
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        cart: state.cart.data,
        favorite: state.favorite.data,
    }
}

export default connect(mapStateToProps)(Header);