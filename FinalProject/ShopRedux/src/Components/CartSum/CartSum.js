import React from 'react';
import './CartSum.css'
import {connect} from "react-redux";

function CartSum({cart, cards}) {

    const priceSum = ((cards.filter((item) => cart.includes(item.name))).map(item => item.price)).reduce((a,b) => a+b, 0)
    return (
        <div className='bay-block'>
        <div>
            <p>Сумма Вашего заказа {priceSum} грн.</p>
        </div>
        <div>
            <a href='/form'><button className='button-form'>Оформить заказ</button></a>
        </div>
        </div>
        );
}
const mapStateToProps = (state) => {
    return {
        cards: state.cards.data,
        cart: state.cart.data,

    }
}


export default connect(mapStateToProps)(CartSum)