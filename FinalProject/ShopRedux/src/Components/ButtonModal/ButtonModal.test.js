import React from "react";
import ButtonModal from "./ButtonModal";
import {render} from "@testing-library/react";

describe ('Testing ButtonModal.js', ()=>{
    test ('Smoke test for ButtonModal.js', ()=>{
        render(<ButtonModal />);
    })

    test ('Title in ButtonModal.js', ()=>{
       const {getByText, getByTestId} = render(<ButtonModal headerModal={'button'}/>);
       getByText('button')
       getByTestId('button-bay')
    })

    test ('Snapshot Testing ButtonModal.js', ()=>{
        const {container} = render(<ButtonModal/>)
        expect(container.innerHTML).toMatchSnapshot()
    })

})