import React from 'react';
import './ButtonModal.css'
import {connect} from "react-redux";

function ButtonModal({setActive, setName, prods, headerModal}) {
    const click = () =>
    {
        setActive(true);
        setName(prods.name)
    }
    return (
        <div>
            <button className='button-bay' data-testid='button-bay' onClick={click}>{headerModal}</button>
        </div>
    );
}



export default ButtonModal;