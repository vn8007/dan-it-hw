import React from "react";
import Modal from "./Modal";
import {render} from "@testing-library/react";


describe ('Testing button in Modal.js', ()=>{


    test ('Test button ok Modal.js', ()=>{
        const {getByText} = render(<Modal/>);
        getByText('ДА')
    })

    test ('Test button no Modal.js', ()=>{
        const {getByText} = render(<Modal/>);
        getByText('НЕТ')
    })

    test ('Test button close Modal.js', ()=>{
        const {getByText} = render(<Modal/>);
        getByText('x')
    })

})

describe ('Snapshot Testing Modal.js', ()=>{
    test('Simple modal snapshot', ()=>{
        const {container} = render(<Modal/>)
        expect(container.innerHTML).toMatchSnapshot()
    })
})


