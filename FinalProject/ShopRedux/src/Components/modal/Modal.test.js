import React from "react";
import Modal from "./Modal";
import {unmountComponentAtNode} from "react-dom";
import ReactDOM from 'react-dom';
import {act} from 'react-dom/test-utils';


let container = null;

beforeEach(()=> {
    container = document.createElement('div');
    document.body.appendChild(container);
})

afterEach(()=> {
    unmountComponentAtNode(container);
    container.remove();
    container=null;
})

describe('Testing Modal.js', ()=> {
    test ('Smoke test for Modal', () =>{
        act(()=>{
            ReactDOM.render(<Modal/>, container)
        })
    })

    test ('Header test for Modal', () =>{
        const headerModal = 'headerModal'
        act(()=>{
            ReactDOM.render(<Modal headerModal={headerModal}/>, container)
        })
        const header = document.getElementById('header')
        expect(header.textContent).toBe(headerModal)
    })
})