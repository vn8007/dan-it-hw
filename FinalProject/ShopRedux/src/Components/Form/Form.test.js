import React from "react";
import Form from "./Form";
import {render} from "@testing-library/react";
import {Provider} from "react-redux";
import store from "../../store";
import userEvent from "@testing-library/user-event";

describe('Tests of Form.js',()=>{
    test ('Smoke test of Form.js', ()=>{
        render(<Provider store={store} ><Form/></Provider>);
    })
    test('Button render submit button of Form.js', ()=>{
        const {getByText, getByTestId} = render(<Provider store={store} ><Form/></Provider>);
        getByTestId('button-submit')
        getByText('Отправить')
    })

    // test ('Button function submit button of Form.js', ()=>{
    //     const mockFunc = jest.fn()
    //     const {getByTestId} = render(<Provider store={store} ><Form/></Provider>);
    //     const button = getByTestId('button-submit')
    //     expect(mockFunc).not.toHaveBeenCalled()
    //     userEvent.click(button)
    //     expect(mockFunc).toHaveBeenCalledTimes(1)
    // })

})