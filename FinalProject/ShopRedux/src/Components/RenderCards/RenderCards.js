import React, {useState} from 'react';
import Icon from "../Star/star";
import DataCard from "../DataCard/DataCard"
import ButtonModal from "../ButtonModal/ButtonModal";
import {star} from "../Theme";
import './RenderCards.css'


function RenderCards({prods, setActive, setName, headerModal}) {
    const [fill, setFill] = useState('white');

    return (
        <div className='card-block-item'>
            <Icon
                prods={prods}
                star={star}
                setFill={setFill}
                fill={fill}
            />
            <DataCard prods={prods}/>
            <ButtonModal
                    prods={prods}
                    setActive={setActive}
                    setName={setName}
                    headerModal={headerModal}
            />
        </div>
    );
}

export default RenderCards;