import React from 'react';
import './DataCard.css'
import {connect} from "react-redux";
import CartIcon from "../CartIcon/CartIcon";

function DataCard({prods, cards, cart}) {
    const counter =
        cart.filter((item) => item === prods.name)

    return (
        <div>
            <img className="img_size" src={prods.imgs[0]}/>
            <p className='card-block-item__product-art'>категория.{prods.category}</p>
            <p className='card-block-item__product-art'>арт.{prods.art}</p>
            <p className='card-block-item__product-name'>{prods.name}</p>
            <p className='card-block-item__product-price'>Цена: {prods.price} грн</p>
            {cart.indexOf(Object.values(prods)[1]) === -1 ? <p> </p> :
                <CartIcon
                    counter={counter}
                />
            }

        </div>
    );
}
const mapStateToProps = (state) => {
    return {
        cards: state.cards.data,
        cart: state.cart.data,

    }
}


export default connect(mapStateToProps)(DataCard);