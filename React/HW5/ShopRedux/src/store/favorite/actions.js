import types from "./types";

const setFavorite = (favoriteData) => ({type: types.SET_FAVORITE, data: favoriteData})

export default {
    setFavorite,
}