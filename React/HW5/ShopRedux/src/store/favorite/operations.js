import actions from "./actions";


const getFavorite = () =>dispatch => {
    try{
        if(!JSON.parse(localStorage.getItem('favorite')))
        {localStorage.setItem('favorite', JSON.stringify([]))}
        dispatch(actions.setFavorite(JSON.parse(localStorage.getItem('favorite'))))
    }
    catch (e){console.log(e)}
}


export default {
    getFavorite,
}
