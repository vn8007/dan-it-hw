import actions from "./actions";


const getCart = () =>dispatch => {try{
    if(!JSON.parse(localStorage.getItem('cart')))
    {localStorage.setItem('cart', JSON.stringify([]))}
    dispatch(actions.setCart(JSON.parse(localStorage.getItem('cart'))))
}
catch (e){console.log(e)}}

export default {
    getCart,
}
