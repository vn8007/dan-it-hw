import React, {useState, useEffect} from 'react';
import RenderCards from "../RenderCards/RenderCards";
import Modal from "../modal/Modal";
import '../../App.css'
import {connect} from "react-redux";
import {cartOperations} from "../../store/Cart";

function Main({cards, dispatch}) {
    const [modalActive, setModalActive] = useState(false);
    const [name, setName] = useState(null);
    const [headerModal, setHeaderModal] = useState('Добавить в корзину');
    const [textModal, setTextModal] = useState('Вы ходите добавить в корзину');

    const cardsRender = (arr) => arr.map(item =>
        <RenderCards key = {item.art}
              prods={item}
              setActive={setModalActive}
              setName={setName}
              headerModal={headerModal}
        />)

    const cartFunc = () => {
        let artCollection = [...(JSON.parse(localStorage.getItem('cart')) || '')]
        artCollection.push(name)
        localStorage.setItem('cart', JSON.stringify(artCollection));
        dispatch(cartOperations.getCart())
        setModalActive(false)
    }



        return (
        <>
        <div className='cards-collection'>
            {cardsRender(cards)}
        </div>
        <Modal
            active={modalActive}
            setActive={setModalActive}
            setName={setName}
            name={name}
            headerModal={headerModal}
            textModal={textModal}
            cartFunc={cartFunc}
        />
        </>
    );
}
const mapStateToProps = (state) => {
    return {
        cards: state.cards.data,
    }
}



export default connect(mapStateToProps)(Main);