import React from 'react';
import {Redirect, Switch, Route} from "react-router-dom";
import Favorite from "../Components/Favorite/Favorite";
import Main from "../Components/Main/Main";
import Cart from "../Components/Cart/cart";
import Form from "../Components/Form/Form"
import {connect} from "react-redux";

function AppRoutes({cart}) {
    return (
        <Switch>

            <Redirect exact from='/' to='/shop'/>

            <Route exact path='/shop'>
                <Main/>
            </Route>

            <Route exact path='/favorite'>
                <Favorite/>
            </Route>

            <Route exact path='/cart'>
                <Cart/>
            </Route>

            <Route exact path='/form'>
                <Form/>
            </Route>

        </Switch>
    );
}
const mapStateToProps = (state) => {
    return {
        cart: state.cart.data,
    }
}

export default connect(mapStateToProps)(AppRoutes);