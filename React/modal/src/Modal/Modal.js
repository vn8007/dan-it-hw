import React, {Component} from 'react';
import Button from "../Button/Button";
import App from "../App";
import './Modal.css'


class Modal extends Component {
    state = {
        text: {
            ok:'ok',
            cancel: 'cancel',
        }
    }

    render() {
        const {header, context, closeModal, active, color} = this.props;
        return (
            <div className={active? "modal active" : "modal"} onClick={closeModal}>
                <div className='modal__content' style={{backgroundColor: color}} onClick={e=>e.stopPropagation()}>
                        <div className='modal__header'>
                        <header>{header.toUpperCase()}</header>
                        <button className={'button_close'} onClick={closeModal}>x</button>
                        </div>
                        <span>{context}</span>
                    <div className={'button__block'}>
                        <Button text={this.state.text.ok.toUpperCase()}
                                closeModal={closeModal}
                                color={color}
                        />
                        <Button text={this.state.text.cancel.toUpperCase()}
                                closeModal={closeModal}
                                color={color}
                        />
                        </div>
                </div>
            </div>
        );
    }
}

export default Modal;