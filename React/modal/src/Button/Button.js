import React, {Component} from 'react';
import './Button.css';
import Modal from "../Modal/Modal";
import App from "../App";


class Button extends Component {
    render() {
       const {text, closeModal, color} = this.props

        return (
            <div>
                <button className={'button__block__btn'} onClick={closeModal} style={{backgroundColor: color}}>{text}</button>
            </div>
        );
    }
}

export default Button;