import React, {Component} from 'react';
import './App.css';
import Modal from "./Modal/Modal";


class App extends Component {
    state = {
        header: ' ',
        context: ' ',
        modalActive: false,
        color: ' ',
    }

    render() {
        const showModal = (event) =>{this.setState({modalActive: true})
            console.log(event.target.id);
            if(event.target.id === '1'){
        this.setState({header: 'top text for window #1', context: 'main text for window #1', color: '#ffffcc'})}
            if(event.target.id === '2'){
        this.setState({header: 'top text for window #2', context: 'main text for window #2', color: '#00cccc'})}
        };


        const closeModal = () =>{this.setState({modalActive: false})};
        // console.log(this.state);
        let {context, header,modalActive, color} = this.state;

        return (
            <div className='App'>
              <button onClick={showModal} id='1'>Open first modal</button>
              <button onClick={showModal} id='2'>Open second modal</button>
                <Modal header={header}
                       context={context}
                       color={color}
                       closeModal={closeModal}
                       active={modalActive}
                />
            </div>
        );
    }
}

export default App;