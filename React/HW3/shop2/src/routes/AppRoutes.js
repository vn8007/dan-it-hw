import React from 'react';
import {Redirect, Switch, Route} from "react-router-dom";
import Favorite from "../Components/Favorite/Favorite";
import Main from "../Components/Main/Main";
import Cart from "../Components/Cart/Cart";

function AppRoutes({products, favorite, setFavorite, cart, setCart}) {

    return (
        <Switch>
            <Redirect exact from='/' to='/shop'/>
            <Route exact path='/shop'>
                <Main products={products}
                      favorite={favorite}
                      setFavorite={setFavorite}
                      carts={cart}
                      setCart={setCart}

                />
            </Route>

            <Route exact path='/favorite'>
                <Favorite products={products}
                          favorite={favorite}
                          setFavorite={setFavorite}
                          cart={cart}
                          setCart={setCart}

                />
            </Route>

            <Route exact path='/cart'>
                <Cart products={products}
                      favorite={favorite}
                      setFavorite={setFavorite}
                      cart={cart}
                      setCart={setCart}

                />
            </Route>
        </Switch>
    );
};

export default AppRoutes;