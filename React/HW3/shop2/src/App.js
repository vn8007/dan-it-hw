import './App.css';
import Header from "./Components/Header/Header";
import AppRoutes from "./routes/AppRoutes";
import {useEffect, useState} from "react";
import axios from "axios";

function App() {
    const [prods, setProds] = useState([]);
    const [favorite, setFavorite] = useState([]);
    const [cart, setCart] = useState([]);




    useEffect(() =>{
            try{
                if(!JSON.parse(localStorage.getItem('favorite')))
                {localStorage.setItem('favorite', JSON.stringify([]))}
                setFavorite(JSON.parse(localStorage.getItem('favorite')))
                }
            catch (e){console.log(e)}
                }, []);

    useEffect(() =>{
            try{
                if(!JSON.parse(localStorage.getItem('cart')))
                {localStorage.setItem('cart', JSON.stringify([]))}
                setCart(JSON.parse(localStorage.getItem('cart')))
            }
            catch (e){console.log(e)}
            }, []);

    useEffect(() => {
        axios('/api/products')
            .then(res => {
                setProds(res.data);
            })
    }, []);

    return (
    <div className="App">
      <Header favorite={favorite}
              cart={cart}/>
      <AppRoutes products={prods}
                 favorite={favorite}
                 setFavorite={setFavorite}
                 cart={cart}
                 setCart={setCart}
      />
    </div>
  );
}

export default App;
