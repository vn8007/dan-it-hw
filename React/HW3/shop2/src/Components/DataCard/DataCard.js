import React from 'react';
import './DataCard.css'

function DataCard({products}) {

    return (
        <div>
            <img src={products.imgs} width='161px' />
            <p className='card-block-item__product-art'>арт.{products.art}</p>
            <p className='card-block-item__product-name'>{products.name}</p>
            <p className='card-block-item__product-price'>Цена: {products.price} грн</p>
            <p className='card-block-item__product-price'>Цвет: {products.color}</p>
        </div>
    );
}

export default DataCard;