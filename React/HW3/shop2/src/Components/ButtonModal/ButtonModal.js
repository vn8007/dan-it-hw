import React from 'react';
import './ButtonModal.css'

function ButtonModal({setActive, setName, products, headerModal}) {
    const click = () =>
    {
        setActive(true);
        setName(products.name)
    }
    return (
        <div>
            <button className='button-bay' onClick={click}>{headerModal}</button>
        </div>
    );
}

export default ButtonModal;