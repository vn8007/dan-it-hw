import React, {useState, useEffect} from 'react';
import Modal from "../modal/Modal";
import RenderCart from "../RenderCart/RenderCart";
import '../../App.css'
import AppRoutes from "../../routes/AppRoutes";

function Cart({products, favorite, setFavorite, cart, setCart}) {
    const [modalActive, setModalActive] = useState(false);
    const [name, setName] = useState(null);
    const [headerModal, setHeaderModal] = useState('Удалить из корзины');
    const [textModal, setTextModal] = useState('Вы ходите удалить из корзины');



    const cartFunc = () => {
        const artCollection =[... JSON.parse(localStorage.getItem('cart'))];

        const collection =
        artCollection.filter(item => {return item !== name});
        localStorage.setItem('cart', JSON.stringify(collection));
        setCart(JSON.parse(localStorage.getItem('cart')));
        setModalActive(false)
        }

    const cards = (arr) => arr.map(item =>
        <RenderCart key = {item.art}
                         products={item}
                         setActive={setModalActive}
                         setName={setName}
                         headerModal={headerModal}
                        favorite={favorite}
                        setFavorite={setFavorite}
        />)

    return (
        <>
            <div className='cards-collection'>
                {(cart===null || cart.length === 0)? <p>В корзине пока пусто</p> :
                    cards(products.filter(item => cart.includes(item.name)))}
            </div>
            <Modal
                products={products}
                active={modalActive}
                setActive={setModalActive}
                setName={setName}
                name={name}
                headerModal={headerModal}
                textModal={textModal}
                cart={cartFunc}
            />
        </>
    );
}

export default Cart;