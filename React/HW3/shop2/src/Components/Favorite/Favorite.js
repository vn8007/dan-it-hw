import React, {useState, useEffect} from 'react';
import RenderFavorites from "../RenderFavorites/RenderFavorites";
import Modal from "../modal/Modal";
import '../../App.css'

function Favorite({products, favorite, setFavorite, setCart}) {
    const [modalActive, setModalActive] = useState(false);
    const [name, setName] = useState(null);
    const [headerModal, setHeaderModal] = useState('Добавить в корзину');
    const [textModal, setTextModal] = useState('Вы ходите добавить в корзину');



    const cards = (arr) => arr.map(item =>
        <RenderFavorites key = {item.art}
                     products={item}
                     setActive={setModalActive}
                     setName={setName}
                     headerModal={headerModal}
                     setFavorite={setFavorite}
                     favorite={favorite}

        />)
    const cart = () => {
        let artCollection = [...(JSON.parse(localStorage.getItem('cart')) || '')]
        artCollection.push(name)
        localStorage.setItem('cart', JSON.stringify(artCollection));
        setCart(JSON.parse(localStorage.getItem('cart')));
        setModalActive(false)
    }

    return (
        <>
        <div className='cards-collection'>
            {(favorite===null || favorite.length === 0)? <p>В избранном пока пусто</p> :
                cards(products.filter(item => favorite.includes(item.art)))}
        </div>
        <Modal
            products={products}
            active={modalActive}
            setActive={setModalActive}
            setName={setName}
            name={name}
            headerModal={headerModal}
            textModal={textModal}
            cart={cart}
        />
        </>
    );
}

export default Favorite;