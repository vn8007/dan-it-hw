import React, {useEffect, useState} from 'react';
import Icon from "../Star/star";
import DataCard from "../DataCard/DataCard"
import ButtonModal from "../ButtonModal/ButtonModal";
import {star} from "../Theme";
import './RenderCards.css'


function RenderCards({products, setActive, setName, headerModal,favorite, setFavorite, setHeaderModal, cart}) {
    const [fill, setFill] = useState('white');

    return (
        <div className='card-block-item'>
            <Icon products={products}
                  star={star}
                  setFill={setFill}
                  fill={fill}
                  setFavorite={setFavorite}
                  favorite={favorite}
            />
            <DataCard products={products}/>
            <ButtonModal setActive={setActive}
                         products={products}
                         setName={setName}
                         headerModal={headerModal}
            />
        </div>
    );
}

export default RenderCards;