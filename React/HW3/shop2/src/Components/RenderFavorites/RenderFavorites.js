import React, {useState} from 'react';
import Icon from "../Star/star";
import {star} from "../Theme";
import DataCard from "../DataCard/DataCard";
import ButtonModal from "../ButtonModal/ButtonModal";
import './RenderFavorite.css'

function RenderFavorites({products, setActive, setName, headerModal, setFavorite,favorite}) {
    const [fill, setFill] = useState('none');

    return (
        <div className='card-block-item'>
            <Icon products={products} star={star}
                  setFill={setFill} fill={fill}
                  setFavorite={setFavorite}
                  favorite={favorite}/>
            <DataCard products={products}/>
            <ButtonModal setActive={setActive}
                         products={products}
                         setName={setName}
                         headerModal={headerModal}/>
        </div>
    );
}

export default RenderFavorites;