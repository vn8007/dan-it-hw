import React, {useState, useEffect} from 'react';
import axios from "axios";
import RenderCards from "../RenderCards/RenderCards";
import Modal from "../modal/Modal";
import '../../App.css'

function Main({products, favorite, setFavorite, setCart, carts}) {
    const [modalActive, setModalActive] = useState(false);
    const [name, setName] = useState(null);
    const [headerModal, setHeaderModal] = useState('Добавить в корзину');
    const [textModal, setTextModal] = useState('Вы ходите добавить в корзину');


    const cards = (arr) => arr.map(item =>
        <RenderCards key = {item.art}
              products={item}
              setActive={setModalActive}
              setName={setName}
              headerModal={headerModal}
              favorite={favorite}
              setFavorite={setFavorite}
        />)

    const cart = () => {
        let artCollection = [...(JSON.parse(localStorage.getItem('cart')) || '')]
        artCollection.push(name)
        localStorage.setItem('cart', JSON.stringify(artCollection));
        setCart(JSON.parse(localStorage.getItem('cart')));
        setModalActive(false)
    }



        return (
        <>
        <div className='cards-collection'>
            {cards(products)}
        </div>
        <Modal
            products={products}
            active={modalActive}
            setActive={setModalActive}
            setName={setName}
            name={name}
            headerModal={headerModal}
            textModal={textModal}
            cart={cart}
        />
        </>
    );
}

export default Main;