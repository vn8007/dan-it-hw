// import express from 'express';
const express = require('express');

const app = express();

const port = 8085;

const products = [
    {art: 111,
        name: 'Куртка рабочая Атис, К5',
        price: 800,
        color: 'grey',
        imgs: '/imgs/1.jpg',
    },
    {art: 112,
        name: 'Полукомбинезон рабочий Атис, К5',
        price: 860,
        color: 'grey',
        imgs: '/imgs/2.jpg',
    },
    {art: 113,
        name: 'Брюки рабочие Атис, К5',
        price: 700,
        color: 'grey',
        imgs: '/imgs/3.jpg',
    },
    {art: 114,
        name: 'Куртка рабочая Гранд СВЛ, К5',
        price: 600,
        color: 'blue',
        imgs: '/imgs/4.jpg',
    },
    {art: 115,
        name: 'Полукомбинезон рабочий Гранд СВЛ, К5',
        price: 620,
        color: 'blue',
        imgs: '/imgs/5.jpg',
    },
    {art: 116,
        name: 'Брюки рабочие Гранд СВЛ, К5',
        price: 480,
        color: 'blue',
        imgs: '/imgs/6.jpg',
    },
    {art: 117,
        name: 'Куртка рабочая Дункан, К6',
        price: 950,
        color: 'lite grey',
        imgs: '/imgs/7.jpg',
    },
    {art: 118,
        name: 'Полукомбинезон рабочий Дункан К6',
        price: 980,
        color: 'lite grey / black',
        imgs: '/imgs/8.jpg',
    },
    {art: 119,
        name: 'Брюки рабочие Дункан, К6',
        price: 800,
        color: 'lite grey / black',
        imgs: '/imgs/9.jpg',
    },
    {art: 120,
        name: 'Куртка рабочая Техно К6',
        price: 750,
        color: 'green',
        imgs: '/imgs/10.jpg',
    }

]
app.get('/api/products', (req, res)=>{res.send(products)})


app.listen(port, ()=> {
    console.log(`Server started on port ${port}`)
})