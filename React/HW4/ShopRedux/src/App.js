import './App.css';
import Header from "./Components/Header/Header";
import AppRoutes from "./routes/AppRoutes";
import {useEffect} from "react";
import {connect} from "react-redux";
import {cardsOperations} from "./store/cards";
import {cartOperations} from "./store/cart";
import {favoriteOperations} from "./store/favorite";


function App({dispatch}) {

    useEffect(() =>{
           dispatch(favoriteOperations.getFavorite())}, []);

    useEffect(() =>{
            dispatch(cartOperations.getCart())
            }, []);

    useEffect(() => {dispatch(cardsOperations.getCards())}, []);


    return (
    <div className="App">
      <Header />
      <AppRoutes
      />
    </div>
  );}

const mapStateToProps = (state) => {
    return {
        favorite: state.favorite.data,
        cart: state.cart.data,

    }
}



export default connect(mapStateToProps)(App);
