import React from 'react';
import {Redirect, Switch, Route} from "react-router-dom";
import Favorite from "../Components/Favorite/Favorite";
import Main from "../Components/Main/Main";
import Cart from "../Components/Cart/Cart";

function AppRoutes() {

    return (
        <Switch>
            <Redirect exact from='/' to='/shop'/>
            <Route exact path='/shop'>
                <Main/>
            </Route>

            <Route exact path='/favorite'>
                <Favorite/>
            </Route>

            <Route exact path='/cart'>
                <Cart/>
            </Route>
        </Switch>
    );
};

export default AppRoutes;