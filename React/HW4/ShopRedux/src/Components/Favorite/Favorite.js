import React, {useState, useEffect} from 'react';
import RenderFavorites from "../RenderFavorites/RenderFavorites";
import Modal from "../modal/Modal";
import '../../App.css'
import {connect} from "react-redux";
import {cartOperations} from "../../store/cart";

function Favorite({cards, favorite, dispatch}) {
    const [modalActive, setModalActive] = useState(false);
    const [name, setName] = useState(null);
    const [headerModal, setHeaderModal] = useState('Добавить в корзину');
    const [textModal, setTextModal] = useState('Вы ходите добавить в корзину');

    console.log(favorite)

    const cardsRender = (arr) => arr.map(item =>
        <RenderFavorites key = {item.art}
                     prods={item}
                     setActive={setModalActive}
                     setName={setName}
                     headerModal={headerModal}

        />)
    const cartFunc = () => {
        let artCollection = [...(JSON.parse(localStorage.getItem('cart')) || '')]
        artCollection.push(name)
        localStorage.setItem('cart', JSON.stringify(artCollection));
        dispatch(cartOperations.getCart())
        setModalActive(false)
    }

    return (
        <>
        <div className='cards-collection'>
            {(favorite.length === 0)? <p>В избранном пока пусто</p> :
                cardsRender(cards.filter(item => favorite.includes(item.art)))}
        </div>
        <Modal
            active={modalActive}
            setActive={setModalActive}
            setName={setName}
            name={name}
            headerModal={headerModal}
            textModal={textModal}
            cartFunc={cartFunc}
        />
        </>
    );
}

const mapStateToProps = (state) => {
    return {
        cards: state.cards.data,
        favorite: state.favorite.data,
    }
}


export default connect(mapStateToProps)(Favorite);