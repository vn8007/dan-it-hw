import React from 'react';
import './DataCard.css'

function DataCard({prods}) {
    return (
        <div>
            <img src={prods.imgs} width='161px' />
            <p className='card-block-item__product-art'>арт.{prods.art}</p>
            <p className='card-block-item__product-name'>{prods.name}</p>
            <p className='card-block-item__product-price'>Цена: {prods.price} грн</p>
            <p className='card-block-item__product-price'>Цвет: {prods.color}</p>
        </div>
    );
}

export default DataCard;