import React, {useState, useEffect} from 'react';
import Modal from "../modal/Modal";
import RenderCart from "../RenderCart/RenderCart";
import '../../App.css'
import {connect} from "react-redux";
import {cartOperations} from "../../store/cart";

function Cart({cards, cart, dispatch}) {
    const [modalActive, setModalActive] = useState(false);
    const [name, setName] = useState(null);
    const [headerModal, setHeaderModal] = useState('Удалить из корзины');
    const [textModal, setTextModal] = useState('Вы ходите удалить из корзины');



    const cartFunc = () => {
        const artCollection =[... JSON.parse(localStorage.getItem('cart'))];

        const collection =
        artCollection.filter(item => {return item !== name});
        localStorage.setItem('cart', JSON.stringify(collection));
        dispatch(cartOperations.getCart())
        setModalActive(false)
        }

    const cardsRender = (arr) => arr.map(item =>
        <RenderCart key = {item.art}
                         prods={item}
                         setActive={setModalActive}
                         setName={setName}
                         headerModal={headerModal}
        />)

    return (
        <>
            <div className='cards-collection'>
                {(cart===null || cart.length === 0)? <p>В корзине пока пусто</p> :
                    cardsRender(cards.filter(item => cart.includes(item.name)))}
            </div>
            <Modal
                active={modalActive}
                setActive={setModalActive}
                setName={setName}
                name={name}
                headerModal={headerModal}
                textModal={textModal}
                cartFunc={cartFunc}
            />
        </>
    );
}

const mapStateToProps = (state) => {
    return {
        cards: state.cards.data,
        cart: state.cart.data,

    }
}


export default connect(mapStateToProps)(Cart);