import React, {useState} from 'react';
import Icon from "../Star/star";
import {star} from "../Theme";
import DataCard from "../DataCard/DataCard";
import ButtonModal from "../ButtonModal/ButtonModal";
import './RenderCart.css'

function RenderCart({prods, setActive, setName, headerModal, favorite, setFavorite}) {
    const [fill, setFill] = useState('none');

    return (
        <div className='card-block-item'>
            <Icon
                prods={prods}
                star={star}
                setFill={setFill}
                fill={fill}
            />
            <DataCard
                prods={prods}
            />
            <ButtonModal
                setActive={setActive}
                prods={prods}
                setName={setName}
                headerModal={headerModal}/>
        </div>
    );
}


export default RenderCart;