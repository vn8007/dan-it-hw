import React from 'react';
import {Route} from "react-router-dom";
import Favorite from "../components/Favorite/Favorite";
import Cart from "../components/Cart/Cart";
import MainProds from "../components/MainProds/MainProds";

function AppRoutes({cards, products, favorite, setSFavorite}) {

    return (
        <>
            <Route path='/shop'>
                <MainProds cards={cards}
                           products={products}
                           />
            </Route>

            <Route path='/favorite'>
                <Favorite cards={cards}
                          products={products}
                          favorite={favorite}
                          setSFavorite={setSFavorite}

                />
            </Route>

            <Route path='/cart'>
                <Cart cards={cards}
                      products={products}
                      />
            </Route>
        </>
    );
};

export default AppRoutes;