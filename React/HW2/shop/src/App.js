import React, {useState, useEffect} from 'react';
import Card from "./components/cards/Card";
import './App.css';
import axios from 'axios';
import Modal from "./components/modal/Modal";
import Header from "./components/Header/Header";
import AppRoutes from "./routes/AppRoutes";

const App =() =>{
    const [prods, setProds] = useState([]);
    const [modalActive, setModalActive] = useState(false);
    const [name, setName] = useState(null);
    const [favorite, setSFavorite] = useState(null);


    useEffect(() => {
            axios('/api/products')
                .then(res => {
                    setProds(res.data);
                })
            }, []);
    useEffect(() =>
        setSFavorite(JSON.parse(localStorage.getItem('favorite'))), []);


    const cards = (arr) => arr.map(item =>
            <Card key = {item.art}
                  products={item}
                  setActive={setModalActive}
                  setName={setName}
            />)
                return (
                    <div className='App'>
                    <Header favorite={favorite}/>
                    <AppRoutes cards={cards}
                               products={prods}
                               name={name}
                               favorite={favorite}
                               setSFavorite={setSFavorite}
                    />
                    <Modal
                            active={modalActive}
                            setActive={setModalActive}
                            products={prods}
                            name={name}
                    />
                    </div>

                );

        }

export default App;