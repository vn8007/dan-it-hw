import React from 'react';


function Favorite({cards, products, favorite}) {


    return (
        <div className='cards-collection'>
            {favorite!=null ? cards(products.filter(item => favorite.includes(item.art))) : <p>В избранном пока пусто</p>}
        </div>
    );
}

export default Favorite;