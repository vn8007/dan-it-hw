import React, {Component} from 'react';
import './Card.css'
import Icon from "../favorites/star";
import {star} from "../../Theme";

class Card extends Component {
    state = {
        fill: 'none'
    }
     setFill =()=> {this.state.fill === 'none'? this.setState({fill: '#ffc107'} ) : this.setState({fill: 'none'} );
    }


    render() {

        const {products, setActive, setName, setSFavorite} = this.props

            return (
            <div className='card-block-item'>
                    <Icon products={products} setSFavorite={setSFavorite} star={star} setFill={this.setFill} fill={this.state.fill}/>
                    <img src={products.imgs} width='161px' />
                    <p className='card-block-item__product-art'>арт.{products.art}</p>
                    <p className='card-block-item__product-name'>{products.name}</p>
                    <p className='card-block-item__product-price'>Цена: {products.price} грн</p>
                    <p className='card-block-item__product-price'>Цвет: {products.color}</p>
                <button className='button-bay' onClick={()=> {setActive(true); setName(products.name)}}>Добавить в корзину</button>
            </div>
        );
    }
}

export default Card;