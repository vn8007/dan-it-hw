import React from 'react';
import './Header.css'


function Header({props}) {
    return (
        <div>
            <header className='background'>
                <div className='top-nav'>
                    <div>
                        <img src='logo192.png' width='50px'/>
                    </div>
                    <div>
                        <ul className='main-menu'>
                            <li className='main-menu__item'>Главная</li>
                            <li className='main-menu__item'>Галерея</li>
                            <li className='main-menu__item'><a href='/shop' className='link-style'>Магазин</a></li>
                            <li className='main-menu__item'>Контакты</li>
                        </ul>
                    </div>
                    <div className='main-menu-bay'>
                        <p className='main-menu-bay__item'><a href='/cart' className='link-style'>корзина</a></p>
                        <p className='main-menu-bay__item'><a href='/favorite' className='link-style'>избранное</a></p>
                    </div>

                </div>
                <h2 className='name'>Магазин спецодежды и средств защиты</h2>
            </header>
        </div>
    );
}

export default Header;