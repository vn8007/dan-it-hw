import React from 'react';
import './star.css'
import {star} from "../../Theme";

const Icon = ({products, fill, setFill}) => {

const fav = () => {
    let artCollection = [...(JSON.parse(localStorage.getItem('favorite')) || '')]
    if(artCollection.find(item => products.art===item)) {
        const collection =
        artCollection.filter(item => {return item !== products.art});
        localStorage.setItem('favorite', JSON.stringify(collection))
    }
    if (!artCollection.find(item => products.art===item))
    {artCollection.push(products.art);
    localStorage.setItem('favorite', JSON.stringify(artCollection))}

    setFill()
}
    return (
        <div className='star-pos' onClick={fav}>
            {star(fill)}
        </div>
    )
};

export default Icon;
