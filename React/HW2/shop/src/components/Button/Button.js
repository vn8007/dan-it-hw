import React, {Component} from 'react';
import './Button.css';


class Button extends Component {
    render() {
       const {text, setActive, products} = this.props
            const card = () => {
                let artCollection = [...(JSON.parse(localStorage.getItem('card')) || '')]
                console.log(artCollection);
                artCollection.push(products.art)
                localStorage.setItem('card', JSON.stringify(artCollection));
                console.log(products)
                console.log(localStorage.getItem('card'));
                setActive(false)
            }

        return (
            <div>
                <button className={'button__block__btn'} onClick={card}>{text}</button>
            </div>
        );
    }
}

export default Button;