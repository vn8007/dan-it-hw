import React, {useEffect, useState} from 'react';

function Cart({cards, products}) {
    const [cart, setCart] = useState('');
    useEffect(() =>
        setCart(JSON.parse(localStorage.getItem('cart'))), []);
    const cleanCart = () =>{
        localStorage.setItem('cart', null);
        setCart(null)
    }
    return (
        <div className>
        <div className='cards-collection'>
            {!cart ? <p>В корзине пока пусто</p> : cards(products.filter(item => cart.includes(item.name)))}
        </div>
            <button className='button__block__btn' onClick={cleanCart}> Очистить корзину</button>
        </div>
    );
}

export default Cart;