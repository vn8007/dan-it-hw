import actions from "./actions";
import reducer from "../cart/reducers";

const initialState = {
    data: []
};

describe('Test cart reducer',() =>{

    const cart = [
       123, 12134]
    test ('SET_CART to data',()=>{
        const action = actions.setCart(cart);
        const newState = reducer(initialState, action)
        expect(newState.data).toBe(cart)
    })

})