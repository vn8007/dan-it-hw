import actions from "./actions";
import reducer from "../cards/reducers";

const initialState = {
    data: []
};

describe('Test cards reducer',() =>{

    const prods = [
        {art: 111
        },
        {art: 112
        }]
    test ('SET_PRODS to data',()=>{
    const action = actions.setCards(prods);
    const newState = reducer(initialState, action)
        expect(newState.data).toBe(prods)
})
})