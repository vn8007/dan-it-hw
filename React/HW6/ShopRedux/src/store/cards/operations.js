import axios from "axios";
import actions from "./actions";


const getCards = () =>dispatch => {axios('/api/products')
    .then(res => {
        dispatch(actions.setCards(res.data));
    })}

export default {
    getCards,
}
