import React from 'react';
import './star.css'
import {star} from "../Theme";
import {connect} from "react-redux";
import {favoriteOperations} from "../../store/favorite";

const Icon = ({prods, fill, setFill, favorite, dispatch}) => {
    setFill(favorite.indexOf(Object.values(prods)[0]) === -1 ? fill : '#ffc107')

    const fav = () => {
    let artCollection = [...(JSON.parse(localStorage.getItem('favorite')) || '')]
    if(artCollection.find(item => prods.art===item)) {
        const collection =
        artCollection.filter(item => {return item !== prods.art});
        localStorage.setItem('favorite', JSON.stringify(collection))
    }
    if (!artCollection.find(item => prods.art===item))
        {artCollection.push(prods.art);
        localStorage.setItem('favorite', JSON.stringify(artCollection))}

    setFill(fill==='white'? '#ffc107': 'white');
        dispatch(favoriteOperations.getFavorite())
    }
    return (
        <div className='star-pos' onClick={fav}>
            {star(fill)}
        </div>
    )
};


const mapStateToProps = (state) => {
    return {
        favorite: state.favorite.data
    }
}
export default connect(mapStateToProps)(Icon);
