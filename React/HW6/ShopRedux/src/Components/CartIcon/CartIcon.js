import React from 'react';
import './CartIcon.css'

function CartIcon({counter}) {
    return (
        <a href='./cart'>
        <div>
            <p className='counter-size'>{counter.length}</p>
        </div>
        <div>
            <img className='icon-size' src='cart.png'/>

        </div>
        </a>
    );
}

export default CartIcon;