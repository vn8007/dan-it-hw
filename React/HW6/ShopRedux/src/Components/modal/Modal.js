import React, {Component} from 'react';
import './Modal.css';

class Modal extends Component {
    state = {
        text: {
            ok:'Да',
            cancel: 'Нет',
        }
    }

    render() {
        const {active, setActive, cartFunc, name, headerModal, textModal} = this.props;
        return (
            <div className={active? "modal active" : "modal"} onClick={()=>setActive(false)}>
                <div className='modal__content' onClick={e=>e.stopPropagation()}>
                        <div className='modal__header'>
                        <header id={'header'}>{headerModal}</header>
                        <button className={'button_close'} onClick={()=>setActive(false)}>x</button>
                        </div>
                        <p className='modal__content__style'>{textModal}</p>
                        <p className='modal__content__style'>{name}?</p>
                    <div className={'button__block'}>
                        <div>
                            <button className={'button__block__btn'} onClick={cartFunc}>{this.state.text.ok.toUpperCase()}</button>
                        </div>
                        <div>
                            <button className={'button__block__btn'} onClick={()=>setActive(false)}>{this.state.text.cancel.toUpperCase()}</button>
                        </div>
                        </div>
                </div>
            </div>
        );
    }
}

export default Modal;