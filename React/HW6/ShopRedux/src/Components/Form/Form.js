import React from 'react';
import './Form.css'
import {Formik} from "formik";
import * as yup from 'yup'
import {cartOperations} from "../../store/cart";
import {connect} from "react-redux";
import NumberFormat from 'react-number-format';


function Form({dispatch, cart}) {
    const validationSchema = yup.object().shape({
        name: yup.string().typeError('Вводите строкой').required('Поле обязательно для ввода'),
        secondName: yup.string().typeError('Вводите строкой').required('Поле обязательно для ввода'),
        age: yup.number().positive('Не может быть отрицательным')
            .min(18, 'Неверный ввод')
            .max(117,'Неверный ввод')
            .typeError('Вводите цифры')
            .required('Поле обязательно для ввода'),
        email: yup.string().email('Введите корректный email').required('Поле обязательно для ввода'),
        address: yup.string().typeError('Вводите строкой').required('Поле обязательно для ввода'),

    })
    const clearCart = () =>
    {
    localStorage.setItem('cart', JSON.stringify([]));
    dispatch(cartOperations.getCart());
    alert('Спасибо за заказ!');
    window.location.href = './shop'
    }
    return (
        <div className='form-wrapper'>
            <Formik initialValues={{
            name: '',
            secondName: '',
            age: '',
            address: '',
            tel:'',
            email:''}
            } validateOnBlur
              onSubmit={(values) => {console.log(values, cart); clearCart()}}
              validationSchema={validationSchema}
            >
                {({values,
                   errors,
                   touched,
                   handleChange,
                   handleBlur,
                   isValid,
                   handleSubmit,
                   dirty})=>(
                       <div className='form-conteiner'>
                           <p className='title'>Оформление заказа</p>
                           <p>
                               <input
                               className='input-form'
                               placeholder={'Введите Ваше имя'}
                               type={'text'}
                               name={'name'}
                               onChange={handleChange}
                               onBlur={handleBlur}
                               value={values.name}
                               />
                           </p>
                           {touched.name && errors.name && <p className='error-massage'>{errors.name}</p>}

                           <p>
                               <input
                                   className='input-form'
                                   placeholder={'Введите Вашу фамилию'}
                                   type={'text'}
                                   name={'secondName'}
                                   onChange={handleChange}
                                   onBlur={handleBlur}
                                   value={values.secondName}
                               />
                           </p>
                           {touched.secondName && errors.secondName && <p className='error-massage'>{errors.secondName}</p>}

                           <p>
                               <input
                                   className='input-form'
                                   placeholder={'Введите Ваш возраст'}
                                   type={'text'}
                                   name={'age'}
                                   onChange={handleChange}
                                   onBlur={handleBlur}
                                   value={values.age}
                               />
                           </p>
                           {touched.age && errors.age && <p className='error-massage'>{errors.age}</p>}

                           <p>
                               <input
                                   className='input-form'
                                   placeholder={'Введите Ваш email'}
                                   type={'email'}
                                   name={'email'}
                                   onChange={handleChange}
                                   onBlur={handleBlur}
                                   value={values.email}
                               />
                           </p>
                           {touched.email && errors.email && <p className='error-massage'>{errors.email}</p>}

                           <p>
                               <NumberFormat
                                   format="+38(###) ###-##-##" mask="_"
                                   className='input-form'
                                   placeholder={'Введите Ваш номер телефона'}
                                   type={'tel'}
                                   name={'tel'}
                                   onChange={handleChange}
                                   onBlur={handleBlur}
                                   value={values.tel}
                               />
                           </p>
                           {touched.tel && errors.tel && <p className='error-massage'>{errors.tel}</p>}

                           <p>
                               <input
                                   className='input-form'
                                   placeholder={'Введите адрес доставки'}
                                   type={'address'}
                                   name={'address'}
                                   onChange={handleChange}
                                   onBlur={handleBlur}
                                   value={values.address}
                               />
                           </p>
                           {touched.address && errors.address && <p className='error-massage'>{errors.address}</p>}


                           <button disabled={!isValid && !dirty}
                                   onClick={handleSubmit}
                                   type={'submit'}
                                   className='button-submit'
                                   data-testid='button-submit'
                           >
                               Отправить</button>

                       </div>
                )}

            </Formik>

        </div>
    );
}
const mapStateToProps = (state) => {
    return {
        cart: state.cart.data,

    }
}
export default connect(mapStateToProps)(Form);