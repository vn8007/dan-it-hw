import React from 'react';
import './DataCard.css'
import {connect} from "react-redux";
import CartIcon from "../CartIcon/CartIcon";

function DataCard({prods, cards, cart}) {
    const counter =
        cart.filter((item) => item === prods.name)

    return (
        <div>
            <img src={prods.imgs} width='161px' />
            <p className='card-block-item__product-art'>арт.{prods.art}</p>
            <p className='card-block-item__product-name'>{prods.name}</p>
            <p className='card-block-item__product-price'>Цена: {prods.price} грн</p>
            <p className='card-block-item__product-price'>Цвет: {prods.color}</p>
            {cart.indexOf(Object.values(prods)[1]) === -1 ? <p> </p> :
                <CartIcon
                    counter={counter}
                />
            }

        </div>
    );
}
const mapStateToProps = (state) => {
    return {
        cards: state.cards.data,
        cart: state.cart.data,

    }
}


export default connect(mapStateToProps)(DataCard);