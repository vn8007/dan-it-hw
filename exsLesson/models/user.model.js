const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    age: Number,
    password: String,
    sex: String,
    profile: {
        username: String,
        firstName: String,
        lastName: String,
        date: Date,
    },
    email: String,
    isMarried: { type: Boolean, default: false },
});

module.exports = mongoose.model('User', UserSchema);