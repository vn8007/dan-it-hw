let btn = document.getElementById('btn');
let menu = document.getElementById('main-menu');
let line1 = document.getElementById('line1');
let line2 = document.getElementById('line2');
let line3 = document.getElementById('line3');

let bntPress = function () {
    menu.classList.toggle('main-menu--invisible')
};
let btnTransform = function() {
    line1.classList.toggle('top-nav-btn__line--turnLeft');
    line2.classList.toggle('top-nav-btn__line--turnRight');
    line3.classList.toggle('invisible')
};
btn.addEventListener('click', bntPress);
btn.addEventListener('click', btnTransform);
